![image](https://i.imgur.com/F1oJQ5N.png) 
# Numberplate Detective
***
Diese App richtet sich an all diejenigen, die sich oftmals fragen aus welchem Landkreis ein Fahrzeug kommt. 
Da man meistens nicht die Möglichkeit hat den Aufkleber zu sehen, welcher sich auf dem Nummernschild befindet und die genaue Herkunft des Fahrzeugs benennt, muss man sich mit dem  Landkreis-Kennzeichen-Kürzel unbefriedigt zufrieden stellen. Zum Beispiel stünde im fiktiven Kennzeichen „B-IZ-9999“ das „B“ für den Landkreis Berlin, jedoch wird es bei dem dem Schild „BLK-IZ-9999“ schon schwieriger. 

Die App wird mithilfe einer Liste und unter der Verwendung von Google Maps den Nutzern und Nutzerinnen nicht nur die Herkunft aufdecken, sondern auch visuell diese auf einer Karte anzeigen können. 
***
### System-Voraussetzungen

| Bezeichnung  | Link  |  
|:-----|:-----:|  
|![icon](https://cdn3.iconfinder.com/data/icons/social-media-logos-flat-colorful/2048/5316_-_Android-20.png)Android System  | [Link](https://www.android.com/)  |  
|![icon](https://cdn1.iconfinder.com/data/icons/all_google_icons_symbols_by_carlosjj-du/16/api-lb.png) Android API 17 und höher  | [Link](https://docs.oracle.com/javaee/5/tutorial/doc/bnbpz.html)  |  
|![icon](https://cdn1.iconfinder.com/data/icons/all_google_icons_symbols_by_carlosjj-du/16/maps_api.png) Aktuellste Google Playservice |[Link](https://play.google.com/store/apps/details?id=com.google.android.gms&hl=en)|  

***  
### Benötigte Rechte zum Ausführen der App  

| Bezeichnung  | Sicherheits-Level  |  Link |  
|:-----|:-----:|:----|  
| INTERNET   |  [normal](https://developer.android.com/reference/android/content/pm/PermissionInfo.html#PROTECTION_NORMAL)![icon](https://cdn3.iconfinder.com/data/icons/fatcow/16/bullet_green.png)  |   [Link](https://developer.android.com/reference/android/Manifest.permission.html#INTERNET)  |  
| ACCESS_NETWORK_STATE | [normal](https://developer.android.com/reference/android/content/pm/PermissionInfo.html#PROTECTION_NORMAL)![icon](https://cdn3.iconfinder.com/data/icons/fatcow/16/bullet_green.png)  |   [Link](https://developer.android.com/reference/android/Manifest.permission.html#ACCESS_NETWORK_STATE) |  
  
  
***  
### Vorschau der App  

Die App besteht aus drei Activities und einem Fragment, wobei letzters lediglich als Spalshscreen verwendet wird. Beim Start der App wird versucht
eine Internetverbingung zu den weiter unten genannten Quellen zu den OpenData Resourcen herzustellen. Besteht keine Internetverbindung wird der Nutzer
mit einem Dialog darauf hingewiesen und kann versuchen den Downloadvorgang erneut zu starten, sobald das Internet auf dem gerät wieder aktiviert/hergestellt wurde.  
Ist der Download fertig, wird der [Splashscreen](https://i.imgur.com/FW3TOE2.png) geschlossen und der Nutzer findet sich auf auf der ersten [Activity](https://i.imgur.com/Z0IWsuf.png)
wieder. Diese zeigt nun eine Liste mit allen in Deutschland vorkommenden Kürzel an ( Verweis auf den Abschnitt OpenData ) auch [Diplomaten-Kennzeichen](https://i.imgur.com/jpQ0uHg.png)
sind dort zu finden. Es besteht die Möglichkeit einer [Suche](https://i.imgur.com/O7KEY0r.png) über die Actionbar im oberen Bereich der Activity. Bei 
Eingabe von Zeichen wird die Liste umgehend mit allen passenden Suchergebnissen aktualisiert.
Gleich neben der Suche gelangt man auch durch ein Icon auf die [zweite Activity](https://i.imgur.com/nLY72oa.png), die etwas über den Nutzen der App beschreibt. 

Die Listenelemente mit den Kürzeln sind touchable, bedeutet bei einer Berührung gelangt man auf die dritte Activity. Diese zeigt entweder Informationen
über ein [nomales Kürzel](https://i.imgur.com/38EV8EN.png) oder über ein [diplomatisches Kürzel](https://i.imgur.com/nuFYjhc.png).  

Zwei weitere Anmerkungen: 
Sollte die Verbindung zu den Resourcen scheitern, wird in der Liste mit den Kürzeln "Downloadfehler" angezeigt. 
Sollte es keine Geocoordinaten zu einem Standort geben, werden die default-Werte 0.000000 für Latitude und Longitude verwendet und der Nutzer
mit einem Dialog darauf hin gewiesen. 
  
***

### Open Data  

Diese App bezieht ihre Intformationen bzw. Daten aus OpenData Quellen. Eine kurze übersetzte Definition lt.  Open Definition ist die folgende:  

"Offene Daten sind Daten, die von jedermann frei benutzt, weiterverwendet und geteilt werden können - die einzige Einschränkung betrifft die Verpflichtung zur Nennung des Urherbers" - [opendatahandbook.org](http://opendatahandbook.org/guide/de/what-is-open-data/)  
  
* Quelle eins: [www.govdata.de](https://www.govdata.de/web/guest/suchen/-/details/kfz-kennzeichen-deutschland) für normale Kennzeichen
* Quelle eins: [www.govdata.de](https://www.govdata.de/web/guest/suchen/-/details/liste-der-diplomaten-kennzeichen-in-d) für diplomatische Kennzeichen  

Auf die verwendeten Daten hat der Entwickler keinen Einfluss und übernimmt auch Verantwortung über die Richtigkeit. 
*** 
## Für Entwickler  
***
### UnitTests, checkstyle, lint, pmd, findbugs und javadoc  
Es wurden insgesamt fünf Testklassen geschrieben, die gegen die vom Entwickler selber geschriebenen Klassen ohne Android-Bibliotheken testen.
Bitte beachten Sie bei der Testklasse [Geocodertest](https://bitbucket.org/tms_ws16_14/numberplatedetective/src/ce7690623e343e20d14a47b7ab20aaf1daf1ec68/NumberplateDetective/app/src/test/java/com/htw_berlin/penomatikus/numberplatedetective/logic/net/GeocoderTest.java?at=master) 
das eine Methode auskommentiert wurde. Es empfiehlt sich diese Klasse einzeln zu testen, da die dort auskommentierte Methode aus unerfindlichen 
Gründen unendlich lange benötigt.  

Bei der gleichzeitigen Verwendung von lint, pmd und checkstyle, können Aneinanderreibungen entstehen, da diese drei mit unter andere Vorstellungen
haben wir der Code am besten auszusehen hat. So wurden an entsprechenden Stellen im Code auch Kommentare geschrieben, die auf eine solche Sittuation
hinweisen.  

Zu den selber geschrieben geschriebenen Klassen ohne Android-Bibliotheken, wurden vollständige JavaDoc's angelegt.  

***  
### Hinweise zum Debugging

Es gibt derzeit leider ein Problem mit der verwendeten mapsAPI2 von Google. Viel mehr liegt das Problem darin, das diese API nur unter der
Verwendung von Google Play Services funktioniert und diese leider nicht auf einem Emulator installiert werden.  
Daher ist es Voraussetztung, dass ein richtiges Android Gerät ab Android API 17 ( JELLY_BEAN_MR1 ) verwendet wird, da sonst die Map zum 
Visualisieren der Daten nicht dargestellt werden kann. Natürlich funktioniert die App auch weiterhin ohne den Play Service von Google ohne 
Abstürzte werden.

Wie bereits mit dem Auftraggeber abgesprochen, wird in den Wochen nach dem Abgabetermin der debug API-Key für die mapAPI2 geupdated, da die 
Lebensdauer dieses Keys auf 30 Tage begrenzt ist.  

Der Entwickler wird in Folge-Projekten auf die Verwendung von GoogleAPIs außerhalb der Androidwelt aufgrund der oben geschilderten Problematik
verzichten.



