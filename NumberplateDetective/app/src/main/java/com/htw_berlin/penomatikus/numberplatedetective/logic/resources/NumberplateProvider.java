package com.htw_berlin.penomatikus.numberplatedetective.logic.resources;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides an object, for holding a list of {@link Numberplate} objects.
 * Furthermore it uses the singleton pattern for gaining the ability to just
 * appear once.
 * Usage:
 * <code>
 *     <pre>
 *         NumberplateProvider provider = NumberplateProvider.createNumberplateProvider();
 *         provider.add(new Numberplate("", "", "", false);
 *     </pre>
 * </code>
 * Created by stefan on 06.01.17.
 */

public final class NumberplateProvider {

    /**
     * Holds all given {@link Numberplate} objects
     */
    private static List<Numberplate> numberplates;

    /**
     * The singleton of {@link NumberplateProvider}
     */
    private static NumberplateProvider self;

    /**
     * Provides an object, for holding a list of {@link Numberplate} objects.
     * Usage:
     * <code>
     *     <pre>
     *         NumberplateProvider provider = NumberplateProvider.createNumberplateProvider();
     *         provider.add(new Numberplate("", "", "", false);
     *     </pre>
     * </code>
     */
    private NumberplateProvider() {
        final int initSize = 250;
        numberplates = new ArrayList<>(initSize);
    }

    /**
     * Creates an {@link NumberplateProvider} object, for holding a list of {@link Numberplate} objects.
     * Usage:
     * <code>
     *     <pre>
     *         NumberplateProvider provider = NumberplateProvider.createNumberplateProvider();
     *         provider.add(new Numberplate("", "", "", false);
     *     </pre>
     * </code>
     * @return @{@link NumberplateProvider#self}
     */
    public static synchronized NumberplateProvider createNumberplateProvider() {
        if (self == null) {
            self = new NumberplateProvider();
        }
        return self;
    }

    /**
     * Adds a new {@link Numberplate} to the {@link NumberplateProvider}. <br>
     * @param newNumberplate The new {@link Numberplate}
     */
    public void receive(Numberplate newNumberplate) {
        numberplates.add(newNumberplate);
    }

    /**
     * @return @{@link NumberplateProvider#numberplates}
     */
    public List<Numberplate> provide() {
        return numberplates;
    }

    /**
     * Clears {@link NumberplateProvider#numberplates}. <br>
     */
    public void clear() {
        numberplates.clear();
    }

}
