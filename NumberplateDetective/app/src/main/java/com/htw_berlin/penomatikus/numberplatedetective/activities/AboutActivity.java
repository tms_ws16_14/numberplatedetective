package com.htw_berlin.penomatikus.numberplatedetective.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.htw_berlin.penomatikus.numberplatedetective.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView content = (TextView) findViewById(R.id.element_activity_about_content);
        content.setMovementMethod(new ScrollingMovementMethod());

    }
}
