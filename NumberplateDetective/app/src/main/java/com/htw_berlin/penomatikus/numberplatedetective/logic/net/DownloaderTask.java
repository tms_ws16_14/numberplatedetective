package com.htw_berlin.penomatikus.numberplatedetective.logic.net;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;

import com.htw_berlin.penomatikus.numberplatedetective.R;
import com.htw_berlin.penomatikus.numberplatedetective.logic.resources.CSVParser;
import com.htw_berlin.penomatikus.numberplatedetective.logic.resources.Numberplate;
import com.htw_berlin.penomatikus.numberplatedetective.logic.resources.NumberplateProvider;

import java.io.IOException;

/**
 * Provides an object, for downloading and parsing both provided open source resources asynchronous
 * to the main thread. Moreover it will notify a provided {@link BaseAdapter}, which is responsible
 * for linking the data-set with a {@link android.widget.ListView}, if the download and parsing
 * processes are finished. This will update the {@link android.widget.ListView} with the new data.
 * <br><b>Usage:</b><br>
 *     <code>
 *         <pre>
 *             DownloaderTask downloaderTask = new DownloaderTask(myBaseAdapter);
 *             downloaderTask.execute();
 *         </pre>
 *     </code>
 * Created by stefan on 08.01.17.
 */

public class DownloaderTask extends AsyncTask<Void, Void, Void> {

    private static final String TAG = "[DownloaderTask]";
    /**
     * Resource of all normal numberplates
     */
    private static final String NORMAL_NUMBERPLATES = "http://www.berlin.de/daten/liste-der-kfz-kennzeichen/kfz-kennz-d.csv";
    /**
     * Resource of all diplomatic numberplates
     */
    private static final String DIPLOMATIC_NUMBERPLATES = "http://www.berlin.de/daten/liste-der-diplomatenkennzeichen/kfz-kennz-diplomat.csv";
    /**
     * Time to sleep in {@link DownloaderTask#sleep()}
     */
    //Suppressed lint warning. When, as lint recommends, this is a local, checkstyle
    //will inform, that this should be declared final...
    @SuppressWarnings("FieldCanBeLocal")
    private final int SLEEP_TIME = 700;
    /**
     * The {@link BaseAdapter} to notify
     */
    private final BaseAdapter listAdapter;

    /**
     *
     */
    private final AppCompatActivity appCompatActivity;

    /**
     * Provides an object, for downloading and parsing both provided open source resources asynchronous
     * to the main thread. Moreover it will notify a provided {@link BaseAdapter}, which is responsible
     * for linking the data-set with a {@link android.widget.ListView}, if the download and parsing
     * processes are finished. This will update the {@link android.widget.ListView} with the new data.
     * <br><b>Usage:</b><br>
     *     <code>
     *         <pre>
     *             DownloaderTask downloaderTask = new DownloaderTask(myBaseAdapter);
     *             downloaderTask.execute();
     *         </pre>
     *     </code>
     * @param listAdapter The {@link BaseAdapter} to notify
     */
    public DownloaderTask(BaseAdapter listAdapter, AppCompatActivity appCompatActivity) {
        super();
        this.listAdapter = listAdapter;
        this.appCompatActivity = appCompatActivity;
    }

    @Override
    protected Void doInBackground(Void... params) {
        int successTaskOne = downloadTask(NORMAL_NUMBERPLATES, "UTF-8", 0);
        int successTaskTwo = downloadTask(DIPLOMATIC_NUMBERPLATES, "ISO-8859-4", 1);

        if (successTaskOne + successTaskTwo != 2) {
            NumberplateProvider provider = NumberplateProvider.createNumberplateProvider();
            provider.clear();
            provider.receive(new Numberplate("Downloadfehler", "", ""));
        } else {
            sleep();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listAdapter.notifyDataSetChanged();
        Log.d("[Downloader Task]", "Downloads finished.");
        // Just hide the fragments view, and let it
        // die with the activity. No need to destroy.
        appCompatActivity.findViewById(R.id.element_activity_main_fragment).setVisibility(View.GONE);
        // Show the apps ActionBar. It was hidden in MainActivity#onCreate.
        if ( appCompatActivity.getSupportActionBar() != null )
            appCompatActivity.getSupportActionBar().show();

    }

    /**
     * Calls {@link Downloader#download(String, String)} to download a resource file. If the download has
     * finished a {@link CSVParser} is used to parse the content of this file and passing the result
     * to a NumberplateProvider ( <i>See {@link CSVParser#parseStringAndAddToProvider()}
     * </i> ).
     * @param url   The path to the resource file
     * @param count The list index of the downloaded file from the {@link Downloader}
     */
    private int downloadTask(String url, String charset, int count) {
        int success = 0;
        if (Downloader.download(url, charset)) {
            Log.d(TAG, "Download file success.");
            CSVParser csvParser = new CSVParser(Downloader.getDownloadContent().get(count));
            try {
                csvParser.parseStringAndAddToProvider();
            } catch (IOException ioe) {
                Log.d(TAG, "An IOException occurred.");
            }
            success = 1;
        } else {
            Log.d(TAG, "Download file error.");
        }
        return success;
    }

    /**
     * Without, the LoadingFragment just popups
     * and immediately hides within an eye blink.
     */
    private void sleep() {
        try {
            Thread.sleep(SLEEP_TIME);
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage());
        }
    }
}

