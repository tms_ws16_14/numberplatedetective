package com.htw_berlin.penomatikus.numberplatedetective.logic.resources;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Provides an object, which serves as model for a numberplate.
 * Created by stefan on 06.01.17.
 */

public final class Numberplate implements Parcelable {

    /**
     * The first letters indicating the origin
     */
    private final String letters;
    /**
     * The related county for the letters
     */
    private final String county;
    /**
     * The related federal state for the letters
     */
    private final String federalState;
    /**
     * Indicates if the numberplate is diplomatic or not.
     */
    private boolean diplomatic;

    /**
     * Provides an object, which serves as model for a numberplate.
     * @param data {@link Numberplate#letters}, {@link Numberplate#county}, {@link Numberplate#federalState}
     *                                        passed within one String array.
     */
    public Numberplate(String... data) {
        letters = ((data[0] != null) ? data[0] : "").trim();
        county = ((data[1] != null) ? data[1] : "").trim();
        if (data.length > 2) {
            federalState = ((data[2] != null) ? data[2] : "").trim();
        } else {
            federalState = "";
        }

        if (letters.matches("[0-9]+")) {
            setDiplomatic();
        }
    }

    /**
     * Provides an object, which serves as model for a numberplate.
     * This constructor is used, for passing a {@link Numberplate} via an {@link android.content.Intent}
     * @param in A Parcel to create a new Numberplate from
     */
    private Numberplate(Parcel in) {

        // checkstyle...
        final int lenght = 4;
        final int indexOfBool = 3;

        String[] data = new String[lenght];

        in.readStringArray(data);
        this.letters = data[0];
        this.county = data[1];
        this.federalState = data[2];
        this.diplomatic = Boolean.valueOf(data[indexOfBool]);
    }

    /**
     * @return @{@link Numberplate#county}
     */
    public String getCounty() {
        return county;
    }


    /**
     * @return @{@link Numberplate#letters}
     */
    public String getLetters() {
        return letters;
    }

    /**
     * @return @{@link Numberplate#federalState}
     */
    public String getFederalState() {
        return federalState;
    }

    /**
     * @return @{@link Numberplate#diplomatic}
     */
    public boolean isDiplomatic() {
        return diplomatic;
    }

    /**
     * Sets @{@link Numberplate#isDiplomatic} to true. <br>
     * ( There is no need to set isDiplomatic to false again )
     */
    public void setDiplomatic() {
        diplomatic = true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(
                new String[] { this.letters, this.county, this.federalState,
                        String.valueOf(this.diplomatic) });
    }

    public static final Creator<Numberplate> CREATOR = new Creator<Numberplate>() {
        @Override
        public Numberplate createFromParcel(Parcel in) {
            return new Numberplate(in);
        }

        @Override
        public Numberplate[] newArray(int size) {
            return new Numberplate[size];
        }
    };

    /**
     * Returns the full data-set of the {@link Numberplate}.
     * @return The full data-set of the {@link Numberplate}
     */
    @SuppressWarnings("unused")
    public String toDebugString() {
        return ":: Numberplate: "
                + letters + " : "
                + county + " : "
                + federalState
                + " : "
                + diplomatic;
    }

    @Override
    public String toString() {
        return letters;
    }
}
