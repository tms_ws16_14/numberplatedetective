package com.htw_berlin.penomatikus.numberplatedetective.logic.net;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.htw_berlin.penomatikus.numberplatedetective.R;
import com.htw_berlin.penomatikus.numberplatedetective.logic.resources.Numberplate;

/**
 * Provides an object to obtain data from a {@link Geocoder} asynchronously and passing
 * those to the provided {@link GoogleMap} via a marker. <br>
 * <b>Usage: </b><br>
 * <code>
 *     <pre>
 *         //within an activity implementing {@link com.google.android.gms.maps.OnMapReadyCallback}
 *         ...
 *
 *         public void onMapReady(GoogleMap googleMap) {
 *             // map is a class member
 *             map = googleMap;
 *             GeocoderTask geocoderTask = new GeocoderTask(map, numberplate);
 *             geocoderTask.execute(numberplate.getCounty(), numberplate.getFederalState());
 *         }
 *     </pre>
 * </code>
 * Created by stefan on 15.01.17.
 */

public class GeocoderTask extends AsyncTask<Void, Void, Void> {

    private final Context context;
    /**
     * The map to show the coordinates of the numberplate
     */
    private final GoogleMap map;
    /**
     * {@link Numberplate} to fetch the lat and lon for
     */
    private final Numberplate numberplate;
    /**
     * A zoom level for the map
     */
    @SuppressWarnings("FieldCanBeLocal")
    //Suppressed lint warning. When, as lint recommends, this is a local, checkstyle
    //will inform, that this should be declared final..
    private final int DETAILED_ZOOM = 3;
    /**
     * A zoom level for the map
     */
    @SuppressWarnings("FieldCanBeLocal")
    //Suppressed lint warning. When, as lint recommends, this is a local, checkstyle
    //will inform, that this should be declared final..
    private final int NORMAL_ZOOM = 10;
    /**
     * Used to store the fetched lat and lon from the {@link Geocoder}
     */
    private String[] cords;

    /**
     * Provides an object to obtain data from a {@link Geocoder} asynchronously and passing
     * those to the provided {@link GoogleMap} via a marker. <br>
     * <b>Usage: </b><br>
     * <code>
     *     <pre>
     *         //within an activity implementing {@link com.google.android.gms.maps.OnMapReadyCallback}
     *         ...
     *
     *         public void onMapReady(GoogleMap googleMap) {
     *             // map is a class member
     *             map = googleMap;
     *             GeocoderTask geocoderTask = new GeocoderTask(map, numberplate);
     *             geocoderTask.execute(numberplate.getCounty(), numberplate.getFederalState());
     *         }
     *     </pre>
     * </code>
     * @param map           The {@link GoogleMap} to pin a marker on
     * @param numberplate   The {@link Numberplate} to get the data from
     */
    public GeocoderTask(GoogleMap map, Context context, Numberplate numberplate) {
        super();
        this.map = map;
        this.context = context;
        this.numberplate = numberplate;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Geocoder geocoder = new Geocoder(numberplate.getCounty(), numberplate.getFederalState());
        cords = geocoder.fetch();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        LatLng latLng = new LatLng(Double.parseDouble(cords[0]), Double.parseDouble(cords[1]));
        addMarker(latLng);
        showInfoDialogIfDefaultCords(latLng);
    }

    /**
     * Adds a pin to {@link GeocoderTask#map} to a specific latitude and longitude position.
     * The pin will get a title, which is the {@link GeocoderTask#numberplate}'s county and
     * letters.
     * @param latLng A specific latitude and longitude position for the pin
     */
    private void addMarker(LatLng latLng) {
        map.addMarker(new MarkerOptions()
                .title(numberplate.getCounty() + " (" + numberplate.getLetters() + ")")
                .position(latLng));
        if (numberplate.isDiplomatic()) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DETAILED_ZOOM));
        } else {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, NORMAL_ZOOM));
        }
    }

    private void showInfoDialogIfDefaultCords(LatLng latLng) {
        LatLng defaultValues = new LatLng(Double.parseDouble(Geocoder.DEFAULT_VALUES[0]),
                Double.parseDouble(Geocoder.DEFAULT_VALUES[1]));
        if (latLng.equals(defaultValues)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.str_geocode_not_found_waring_for_user)
                    .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.i("Geocoderdialog", "User clicked OK");
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

}
