package com.htw_berlin.penomatikus.numberplatedetective.logic.listeners;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.htw_berlin.penomatikus.numberplatedetective.activities.NumberplateActivity;
import com.htw_berlin.penomatikus.numberplatedetective.logic.resources.Numberplate;

import java.util.Locale;

/**
 * Provides an object implementing AdapterView.OnItemClickListener to full fill certain operations
 * on a {@link android.widget.ListView} using data from a NumberplateProvider.
 * <br><br><b>Note:</b>
 * This class is written to keep the MainActivity clean of extra code. <br><br>
 * Created by stefan on 09.01.17.
 */

public class CustomItemClickListener implements AdapterView.OnItemClickListener {

    /**
     *  The {@link Activity} using this listener
     */
    private final Activity caller;

    /**
     * Provides an object implementing AdapterView.OnItemClickListener to full fill certain operations
     * on a {@link android.widget.ListView} using data from a NumberplateProvider.
     * <br><br><b>Note:</b>
     * This class is written to keep the MainActivity clean of extra code. <br><br>
     * @param caller    The {@link Activity} using this
     */
    public CustomItemClickListener(Activity caller) {
        this.caller = caller;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Numberplate numberplate = (Numberplate) parent.getAdapter().getItem(position);

        if (isNumberPlate(numberplate)) {
            Intent intent = new Intent(caller.getBaseContext(), NumberplateActivity.class);
            intent.putExtra("Numberplate", numberplate);
            caller.startActivity(intent);
        }
    }

    /**
     * Checks if the provided {@link Numberplate} is real or just the headline of the content like
     * "Kennzeichen" or "Diplomatische-kennzeichen" or "Downloadfehler.
     * @param numberplate The {@link Numberplate} to check
     * @return True if its a real one
     */
    private boolean isNumberPlate(Numberplate numberplate) {
        boolean kennzeichen = !numberplate.getLetters().toLowerCase(Locale.GERMAN).
                contains("kennzeichen");
        boolean downloadfehler = !numberplate.getLetters().toLowerCase(Locale.GERMAN).
                contains("downloadfehler");
        return downloadfehler && kennzeichen;
    }
}
