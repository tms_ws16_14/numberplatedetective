package com.htw_berlin.penomatikus.numberplatedetective.logic.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides an object for downloading a web resource. <br>
 * Stores the downloaded resource as String into a {@link List}.<br>
 * <b>Usage:</b><br>
 * <code>
 *     <pre>
 *     if ( Downloader.download("http://www.myresource.com/file.txt") )
 *          Log.i("[Download Task]", "Download successful");
 *     String myWebResource = Downloader.getContent().get(0);
 *     </pre>
 * </code>
 * Created by stefan on 08.01.17.
 */
final class Downloader {

    /**
     * Stores all downloaded content as String
     */
    private static final List<String> DOWNLOAD_CONTENT = new ArrayList<>();

    private Downloader() { }

    /**
     * Downloads a given web resource and stores the content as String. <br>
     * <b>Usage:</b><br>
     * <code>
     *     <pre>
     *     if ( Downloader.download("http://www.myresource.com/file.txt") )
     *          Log.i("[Download Task]", "Download successful");
     *     String myWebResource = Downloader.getContent().get(0);
     *     </pre>
     * </code><br>
     *
     * @param downloadURL   The path to the web resource
     * @return  True if the download was successful
     */
    public static boolean download(String downloadURL, String charset) {
        boolean success;
        if (!"UTF-8".equals(charset) && !"ISO-8859-4".equals(charset)) {
            return false;
        }

        try {
            URLConnection connection = new URL(downloadURL).openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(),  Charset.forName(charset)));

            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine).append("\n ");
            }

            DOWNLOAD_CONTENT.add(response.toString());
            in.close();
            success = true;
        } catch (MalformedURLException e) {
            success = false;
            System.err.println("Downloader: The URL was false.");
        } catch (IOException e) {
            success = false;
            System.err.println("Downloader: an IOException occurred.");
        }
        return success;
    }

    /**
     * @return @{@link Downloader#DOWNLOAD_CONTENT}
     */
    public static List<String> getDownloadContent() {
        return Downloader.DOWNLOAD_CONTENT;
    }
}