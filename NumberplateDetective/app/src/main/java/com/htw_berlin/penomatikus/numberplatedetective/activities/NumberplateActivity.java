package com.htw_berlin.penomatikus.numberplatedetective.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;

import com.htw_berlin.penomatikus.numberplatedetective.R;
import com.htw_berlin.penomatikus.numberplatedetective.logic.net.GeocoderTask;
import com.htw_berlin.penomatikus.numberplatedetective.logic.resources.Numberplate;

public class NumberplateActivity extends AppCompatActivity implements OnMapReadyCallback {

    private Numberplate numberplate;
    private MapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numberplate);

        Bundle extras = getIntent().getExtras();
        numberplate = extras.getParcelable("Numberplate");

        TextView shortDes = (TextView) findViewById(R.id.element_numberplate_data_short_des);
        shortDes.setText(R.string.str_numberplate_data_short_des);

        TextView sh0rt = (TextView) findViewById(R.id.element_numberplate_data_short);
        sh0rt.setText(numberplate.getLetters());

        TextView countyDes = (TextView) findViewById(R.id.element_numberplate_data_county_des);
        countyDes.setText(R.string.str_numberplate_data_county_des);

        TextView county = (TextView) findViewById(R.id.element_numberplate_data_county);
        county.setText(numberplate.getCounty());

        TextView federalDes = (TextView) findViewById(R.id.element_numberplate_data_federal_des);
        federalDes.setText(R.string.str_numberplate_data_federal_des);

        TextView federal = (TextView) findViewById(R.id.element_numberplate_data_federal);
        federal.setText(numberplate.getFederalState());

        // in case of a diplomatic numberplate,
        // rename county description and hide federal texts
        if (numberplate.isDiplomatic()) {
            federalDes.setVisibility(View.GONE);
            federal.setVisibility(View.GONE);
            countyDes.setText(R.string.str_numberplate_data_county_des_diplomatic);
        }

        mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.element_numberplate_map);
        mapFragment.getMapAsync(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        GeocoderTask geocoderTask = new GeocoderTask(googleMap, NumberplateActivity.this, numberplate);
        geocoderTask.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mapFragment != null) {
            mapFragment.onResume();
        }
    }

}

