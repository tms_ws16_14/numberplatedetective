package com.htw_berlin.penomatikus.numberplatedetective.logic.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

/**
 * Provides an object, to fetch latitude and longitude for a
 * {@link com.htw_berlin.penomatikus.numberplatedetective.logic.resources.Numberplate}'s county
 * and federal state ( or just country of origin if diplomatic ). Therefor it's using the free
 *  ( <a href="https://nominatim.openstreetmap.org">REST-Api "nominatim" of openstreetmap.org</a>.
 * <br><b>Usage: </b><br>
 * <code>
 *      <pre>
 *          Numberplate numberplate = new Numberplate(myStringArrayHoldingInfosOfAnumberplate);
 *          Geocoder geocoder = new Geocoder(numberplate.getCounty(), numberplate.getFederalState());
 *          String[] cords = geocoder.fetch();
 *      </pre>
 * </code>
 * <br>Created by stefan on 15.01.17.
 */

class Geocoder {

    /**
     * The county to pass to the REST-Api
     */
    private final String county;
    /**
     * The federal state to pass to the REST-Api
     */
    private final String federalState;
    /**
     * The result of the REST-Api as
     */
    private String xmlResponse;
    /**
     * Default latitude and longitude values
     */
    public static final String[] DEFAULT_VALUES = new String[]{ "0.0000000", "0.0000000" };


    /**
     * Provides an object, to fetch latitude and longitude for a
     * {@link com.htw_berlin.penomatikus.numberplatedetective.logic.resources.Numberplate}'s county
     * and federal state ( or just country of origin if diplomatic ). Therefor it's using the free
     *  ( <a href="https://nominatim.openstreetmap.org">REST-Api "nominatim" of openstreetmap.org</a>.
     * <br><b>Usage: </b><br>
     * <code>
     *      <pre>
     *          Numberplate numberplate = new Numberplate(myStringArrayHoldingInfosOfAnumberplate);
     *          Geocoder geocoder = new Geocoder(numberplate.getCounty(), numberplate.getFederalState());
     *          String[] cords = geocoder.fetch();
     *      </pre>
     * </code>
     * @param county        The county to fetch
     * @param federalState  The federalState fetch
     */
    public Geocoder(String county, String federalState) {
        this.county = county;
        this.federalState = federalState;
    }

    /**
     * Builds an URL working to the REST-Api
     * ( <b>see</b> <a href="https://wiki.openstreetmap.org/wiki/Nominatim"> Nominatim Wiki ) </a>
     * and returns the latitude and longitude values of the county, federal state or country of
     * origin if diplomatic. <br>
     * <b>Note: </b> <br>
     * If the REST-Api result could't be downloaded or does not provide the expected results,
     * the latitude and longitude values "0.0000000", "0.0000000" will be returned.
     * @return latitude and longitude values of the county, federal state or country of
     * origin if diplomatic or "0.0000000", "0.0000000".
     */
    public String[] fetch() {
        String restSearch = "search.php?q=";
        String restQuery = county + "," + federalState;
        String restFormat = "&format=xml";
        String restResultLimit = "&limit=1";
        String nominatimOpenstreetmapREST = "https://nominatim.openstreetmap.org/"
                + restSearch
                + restQuery
                + restFormat
                + restResultLimit;

        if (download(nominatimOpenstreetmapREST)) {
            return readCoordinates();
        } else {
            return DEFAULT_VALUES;
        }
    }

    /**
     * Connects to the provided REST-Api URL and downloads the result.
     * @param nominatimOpenstreetmapREST    the provided REST-Api URL
     * @return  true if the download was successful
     */
    private boolean download(String nominatimOpenstreetmapREST) {
        boolean success;
        try {
            URLConnection connection = new URL(nominatimOpenstreetmapREST).openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(), Charset.forName("UTF-8")));

            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
                response.append("\n ");
            }
            in.close();
            xmlResponse = response.toString();
            success = true;
        } catch (MalformedURLException e) {
            success = false;
            System.err.println("Geocoder: The URL was false.");
        } catch (IOException e) {
            success = false;
            System.err.println("Geocoder: an IOExeption occurred.");
        }
        return success;
    }

    /**
     * This method will parse the XML response of the REST-Api and returning either
     * {@link Geocoder#DEFAULT_VALUES} if something bad happens while reading the XML, for example
     * the searched county does not exists, or the provided latitude and longitude values as String[].
     * <br>
     * It might be helpful to understand the code, when pointing out how the response looks like:
     * <br><b>Example of XML response: </b><br>
     * The XML response when the URL
     * <a href="https://nominatim.openstreetmap.org/search.php?q=Augsburg,Bayern&format=xml&limit=1">
     * https://nominatim.openstreetmap.org/search.php?q=Augsburg,Bayern&format=xml&limit=1</a>: <br>
     * <code>
     *     <pre>
     *         searchresults <br>
     *             timestamp="Sun, 15 Jan 17 21:21:57 +0000" <br>
     *             attribution="Data © OpenStreetMap contributors, ODbL 1.0. http://www.openstreetmap.org/copyright"<br>
     *             querystring="Augsburg,Bayern"<br>
     *             polygon="false"
     *             exclude_place_ids="158686125"
     *             more_url="https://nominatim.openstreetmap.org/search.php.......(shorted)"> <br>
     *             place <br>
     *                  place_id="158686125"<br>
     *                  osm_type="relation"<br>
     *                  osm_id="62407"<br>
     *                  place_rank="16"<br>
     *                  boundingbox="48.2581444,48.4586541,10.7633615,10.9593328" <br>
     *                  lat="48.3668041"<br>
     *                  lon="10.8986971"<br>
     *                  display_name="Augsburg, Schwaben, Bayern, Deutschland"<br>
     *                  class="place"<br>
     *                  type="city"<br>
     *                  importance="0.86728191471083"<br>
     *                  icon="https://nominatim.openstreetmap.org/images/mapicons/poi_place_city.p.20.png"/><br>
     *          /searchresults
     *     </pre>
     * </code>
     * @return Either {@link Geocoder#DEFAULT_VALUES} or latitude and longitude values
     */
    private String[] readCoordinates() {
        String[] coordinates = new String[2];

        // See comment to understand
        int latIndex = xmlResponse.indexOf("lat=");
        int displayNameIndex = xmlResponse.indexOf("display_name=");

        boolean success = false;
        try {
            // will get the String: lat="48.3668041" lon="10.8986971"
            String[] substring = xmlResponse.substring(latIndex, displayNameIndex).split(" ");
            // will get the String: 48.3668041 out of lat="48.3668041"
            coordinates[0] = substring[0].substring(substring[0].indexOf('\'') + 1, substring[0].lastIndexOf('\''));
            // will get the String: 10.8986971 out of lon="10.8986971"
            coordinates[1] = substring[1].substring(substring[1].indexOf('\'') + 1, substring[1].lastIndexOf('\''));
            // all values are found
            success = true;
        } catch (StringIndexOutOfBoundsException e) {
           System.err.println("Geoocder: Ignore exception handling. Use default values for coordinates now.");
        }

        return (success && isLatLon(coordinates)) ? coordinates : DEFAULT_VALUES;
    }

    /**
     * Checks the syntax of the values for lat or lon correctness.<br>
     * <b>From: </b> <a href="http://stackoverflow.com/a/22903890">stackoverflow</a>
     * @param values    Coordinates
     * @return  True if correct
     */
    private boolean isLatLon(String... values) {
        String lat = values[0];
        String lon = values[1];
        String regex = "^[-]?[0-9]+\\.[0-9]+";
        boolean latBool = lat.matches(regex);
        boolean lonBool = lon.matches(regex);

        return (latBool && lonBool);
    }
}
