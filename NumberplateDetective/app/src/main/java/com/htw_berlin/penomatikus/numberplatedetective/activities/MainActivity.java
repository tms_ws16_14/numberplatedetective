package com.htw_berlin.penomatikus.numberplatedetective.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.htw_berlin.penomatikus.numberplatedetective.R;
import com.htw_berlin.penomatikus.numberplatedetective.fragments.LoadingFragment;
import com.htw_berlin.penomatikus.numberplatedetective.logic.adapters.CustomFilterableAdapter;
import com.htw_berlin.penomatikus.numberplatedetective.logic.listeners.CustomItemClickListener;
import com.htw_berlin.penomatikus.numberplatedetective.logic.net.DownloaderTask;
import com.htw_berlin.penomatikus.numberplatedetective.logic.resources.NumberplateProvider;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        LoadingFragment.OnFragmentInteractionListener {

    private CustomFilterableAdapter listViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // hide for fragment and show later via DownloaderTask#onPreExecute
        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        setContentView(R.layout.activity_main);
        initUiIfInternetConnectionExists(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.action_bar_search_field);
        SearchView searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);

        menu.findItem(R.id.action_bar_about_button).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(getBaseContext(), AboutActivity.class));
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listViewAdapter.getFilter().filter(newText);
        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        // have to implement this method for the use of
        // fragments. However, I need to write code in here.
        // The final bool is used to bypass the checkstyle/pmd warning of
        // an empty method;
        @SuppressWarnings("UnusedAssignment")
        final boolean thisIsNotAnEmptyMethodAnymore = true;
    }

    /**
     * Builds the UI of the App
     * @param savedInstanceState a Bundle object containing the activity's previously saved state
     */
    private void initUiIfInternetConnectionExists(Bundle savedInstanceState) {

        if (isInternetAvailable()) {
            NumberplateProvider provider = NumberplateProvider.createNumberplateProvider();
            listViewAdapter = new CustomFilterableAdapter(this, provider.provide());

            DownloaderTask downloaderTask = new DownloaderTask(listViewAdapter, this);
            downloaderTask.execute();

            ListView listView = (ListView) findViewById(R.id.element_activity_main_customListView);
            listView.setAdapter(listViewAdapter);
            listView.setOnItemClickListener(new CustomItemClickListener(this));

            if (savedInstanceState == null) {
                LoadingFragment loadingFragment = LoadingFragment.newInstance();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.element_activity_main_fragment, loadingFragment)
                        .commitNow();
            }
        } else {
            showNoConnectionAlert(savedInstanceState);
        }
    }

    /**
     * Shows an {@link AlertDialog} which tells the user, that
     * no internet connection could be established, and if he/she wants
     * to try again.
     * @param savedInstanceState a Bundle object containing the activity's previously saved state
     */
    private void showNoConnectionAlert(Bundle savedInstanceState) {
        final Bundle savedInstanceStateCopy = savedInstanceState;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.str_mainactivity_no_internet_warning)
                .setCancelable(false)
                .setPositiveButton("Wiederholen", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        initUiIfInternetConnectionExists(savedInstanceStateCopy);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Checks if the android device is connected to a network.
     * Does not check, if the device really can connect to a webpage.
     * @return  True, if the android device is connected to a network.
     */
    private boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
