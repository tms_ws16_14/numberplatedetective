package com.htw_berlin.penomatikus.numberplatedetective.logic.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * Provides an object to parse a CSV, provided as String resource, for Numberplate data. <br>
 * Furthermore, the parsed data will be added to the {@link NumberplateProvider}, which is used
 * to provide those data for later usage.<br>
 * <b>Usage:</b>
 * <code>
 *     <pre>
 *         // To parse a resource
 *         CSVParser parser = new CSVParser(myCSVAsString);
 *         parser.parseStringAndAddToProvider();
 *         // To get the date
 *         ArrayList<Numberplates> data = provider.provide();
 *     </pre>
 * </code>
 * <br>
 * <b>Note:</b><br>
 * This class is not a real adaptable parser for any CSV files. It is written to work perfectly for
 * the provided CSV files, used for this app, which can be found in
 * {@link com.htw_berlin.penomatikus.numberplatedetective.logic.net.DownloaderTask}. <br> However,
 * its simple implementation increases its maintainability. It should be easy to refactor
 * the {@link CSVParser}, in case of needs. <br>
 * Created by stefan on 06.01.17.
 */

public class CSVParser {

    /**
     * The separator used in the CSV files.
     */
    private static final String SEPERATOR = ",";
    /**
     * Holds the data of the CSV file
     */
    private final String csvAsString;
    /**
     * Used to store the found numberplate data
     */
    private static final NumberplateProvider PROVIDER = NumberplateProvider.createNumberplateProvider();

    /**
     * Provides an object to parse a CSV, provided as String resource, for Numberplate data. <br>
     * Furthermore, the parsed data will be added to the {@link NumberplateProvider}, which is used
     * to provide those data for later usage.<br>
     * <b>Usage:</b>
     * <code>
     *     <pre>
     *         // To parse a resource
     *         CSVParser parser = new CSVParser(myCSVAsString);
     *         parser.parseStringAndAddToProvider();
     *         // To get the date
     *         ArrayList<Numberplates> data = provider.provide();
     *     </pre>
     * </code>
     * <br>
     * @param csvAsString   The CSV file as String
     */
    public CSVParser(String csvAsString) {
        this.csvAsString = csvAsString;
     }

    /**
     * Removes all " and all invisible control characters and unused code points
     * from the input string.
     * @param input The String to clear
     */
    private String removeSpecialCharactersFromString(String input) {
         return input.replace("\"", "").replace("\\p{C}", "");
    }

    /**
     *  Parses {@link CSVParser#csvAsString} and adds all new found {@link Numberplate}s to the {@link NumberplateProvider}.
     *  The CSV will be read line by line. {@link CSVParser#removeSpecialCharactersFromString(String)}
     *  is called on every line, to ensure that no unintentional character will find its way to a
     *  new {@link Numberplate}.
     *
     * @throws IOException  If an I/O error occurs.
     */
    public void parseStringAndAddToProvider() throws IOException {
        BufferedReader reader = new BufferedReader(new StringReader(csvAsString));
        String line;
        while ((line = reader.readLine()) != null) {
            line = removeSpecialCharactersFromString(line);
            String[] tmp = line.split(SEPERATOR);
            if (tmp.length >= 2) {
                PROVIDER.receive(new Numberplate(tmp));
            }
        }
    }

}
