package com.htw_berlin.penomatikus.numberplatedetective.logic.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.htw_berlin.penomatikus.numberplatedetective.R;
import com.htw_berlin.penomatikus.numberplatedetective.activities.MainActivity;
import com.htw_berlin.penomatikus.numberplatedetective.logic.resources.Numberplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Provides a custom {@link BaseAdapter} implementing {@link Filterable}, which
 * which makes the {@link android.widget.ListView} searchable, using it. <br>
 * <b>Usage:</b><br>
 *     <code>
 *         <pre>
 *             // In onCreate()
 *             listViewAdapter = new CustomFilterableAdapter(this, myListOfNumberPlates);
 *             ListView listView = (ListView) findViewById(R.id.customListView);
 *             listView.setAdapter(listViewAdapter);
 *
 *             // In Activity implementing SearchView.OnQueryTextListener
 *             ...
 *             public boolean onQueryTextChange(String newText) {
 *                  listViewAdapter.getFilter().filter(newText);
 *                  return false;
 *             }
 *         </pre>
 *     </code>
 *
 * Created by stefan on 11.01.17.
 */

public class CustomFilterableAdapter extends BaseAdapter implements Filterable {

    /**
     * Filter for the search within the data
     */
    private final NumberplateFilter numberplateFilter;
    /**
     * Source data of {@link Numberplate}s
     */
    private final List<Numberplate> numberplates;

    /**
     * Filtered list of {@link Numberplate}s
     */
    private List<Numberplate> filteredNumberplates;

    private final MainActivity activity;

    public CustomFilterableAdapter(MainActivity activity, List<Numberplate> numberplates) {
        super();
        this.activity = activity;
        this.numberplates = numberplates;
        this.filteredNumberplates = numberplates;
        this.numberplateFilter = new NumberplateFilter();
    }

    /**
     * Get size of user list
     * @return userList size
     */
    @Override
    public int getCount() {
        return filteredNumberplates.size();
    }

    /**
     * Get specific item from the filtered list
     * of {@link Numberplate} objects
     * @param i item index
     * @return list item
     */
    @Override
    public Object getItem(int i) {
        return filteredNumberplates.get(i);
    }

    /**
     * Get item id from the filtered list
     * of {@link Numberplate} objects
     * @param position item index
     * @return current item id
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        Numberplate numberplate = (Numberplate) getItem(position);

        if (convertView == null) {
            LayoutInflater layoutInflater =
                    (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder();
            holder.letters = (TextView) convertView.findViewById(R.id.element_list_item_data_text);
            holder.letters.setText(numberplate.getLetters());

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.letters.setText(numberplate.getLetters());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return numberplateFilter;
    }


    /**
     * Viewholder-pattern, to avoid extra calls of findViewById. <br>
     * <b>From:</b> <a href="https://developer.android.com/training/improving-layouts/smooth-scrolling.html">
     *     Android Developer </a>
     * "A ViewHolder object stores each of the component views inside the tag field of the Layout,
     * so you can immediately access them without the need to look them up repeatedly."
     */
    private static class ViewHolder {
        TextView letters;
    }

    /**
     * Provides a custom {@link Filter} for filtering the provided data of
     * {@link CustomFilterableAdapter} and updates its view with the filtered data, if found any.
     */
    private class NumberplateFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<Numberplate> tmp = new ArrayList<>();

                //can't use, Convert2streamapi because of min API 17 ( 24 required )
                //noinspection Convert2streamapi
                for (Numberplate n : numberplates) {
                    if (n.getLetters().toLowerCase(Locale.GERMAN).contains(
                            constraint.toString().toLowerCase(Locale.GERMAN))) {
                        tmp.add(n);
                    }
                }
                results.count = tmp.size();
                results.values = tmp;
            } else {
                results.count = numberplates.size();
                results.values = numberplates;
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredNumberplates = (ArrayList<Numberplate>) results.values;
            notifyDataSetChanged();
        }
    }
}