package com.htw_berlin.penomatikus.numberplatedetective.logic.net;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;

/**
 * Test for the Downloader class.
 * All warnings are suppressed because this is a test
 * Created by stefan on 16.01.17.
 */
@SuppressWarnings("ALL")
public class DownloaderTest {

    private static final String TEST_URL = "http://www.berlin.de/daten/liste-der-kfz-kennzeichen/kfz-kennz-d.csv";

    @Before
    public void resetStaticDownloadList() throws NoSuchFieldException, IllegalAccessException {
        Field instance = Downloader.class.getDeclaredField("DOWNLOAD_CONTENT");
        instance.setAccessible(true);
        @SuppressWarnings("unchecked")
        ArrayList<String> downloadContent = (ArrayList<String>) instance.get(this);
        downloadContent.clear();
    }

    @Test
    public void downloadPositiv() throws Exception {
        boolean iso = Downloader.download(TEST_URL, "ISO-8859-4");
        boolean utf = Downloader.download(TEST_URL, "UTF-8");
        assertEquals(true, iso);
        assertEquals(true, utf);
    }

    @Test
    public void getDownloadContent() throws Exception {
        Downloader.download(TEST_URL, "ISO-8859-4");
        int result = Downloader.getDownloadContent().size();
        assertEquals(1, result);
    }

}