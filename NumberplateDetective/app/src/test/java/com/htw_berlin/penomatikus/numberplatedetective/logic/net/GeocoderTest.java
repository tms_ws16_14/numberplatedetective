package com.htw_berlin.penomatikus.numberplatedetective.logic.net;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Test for the Geocoder class
 * All warnings are suppressed because this is a test
 * Created by stefan on 16.01.17.
 */
@SuppressWarnings("ALL")
public class GeocoderTest {

    @Test
    public void fetchPositive() throws Exception {
        // Used rest-api call:
        // https://nominatim.openstreetmap.org/search.php?q=V%C3%B6lklingen%2C+Saarland&format=xml&limit=1
        Geocoder geocoder = new Geocoder("Völklingen", "Saarland");
        String[] cords = geocoder.fetch();
        assertEquals("49.2399807", cords[0]);
        assertEquals("6.8460114", cords[1]);
    }


    // I don't know why, but this tests might take for ever.
    // Kill gradle and remove this test from the class ( comment ).
    // Then start again.
    // This issue does not appear in the product, just in this test.
//    @Test
//    public void fetchNegative() throws Exception {
//        Geocoder geocoder = new Geocoder("not a county", "not a federal state");
//        String[] cords = geocoder.fetch();
//        assertEquals("0.0000000", cords[0]);
//        assertEquals("0.0000000", cords[1]);
//    }

}