package com.htw_berlin.penomatikus.numberplatedetective.logic.resources;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;


/**
 * Test for the NumberplateProvider class
 * All warnings are suppressed because this is a test
 * Created by stefan on 16.01.17.
 */
@SuppressWarnings("ALL")
public class NumberplateProviderTest {

    @Before
    public void resetSingleton() throws NoSuchFieldException, IllegalAccessException {
        Field instance = NumberplateProvider.class.getDeclaredField("self");
        instance.setAccessible(true);
        instance.set(null, null);
    }

    @Test
    public void createNumberplateProvider() throws Exception {
        NumberplateProvider provider = NumberplateProvider.createNumberplateProvider();
        assertNotNull(provider);
    }

    @Test
    public void receive() throws Exception {
        NumberplateProvider provider = NumberplateProvider.createNumberplateProvider();
        provider.receive(new Numberplate("", "", ""));

        Field instance = NumberplateProvider.class.getDeclaredField("numberplates");
        instance.setAccessible(true);

        @SuppressWarnings("unchecked")
        ArrayList<Numberplate> reflectedList = (ArrayList<Numberplate>) instance.get(this);

        int resultSize = reflectedList.size();
        assertEquals(1, resultSize);

    }

    @Test
    public void provide() throws Exception {
        NumberplateProvider provider = NumberplateProvider.createNumberplateProvider();
        provider.receive(new Numberplate(new String[]{"", "", ""}));
        int resultSize = provider.provide().size();
        assertEquals(1, resultSize);
    }

}