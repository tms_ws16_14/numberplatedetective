package com.htw_berlin.penomatikus.numberplatedetective.logic.resources;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Test for the CSVParser class
 * All warnings are suppressed because this is a test
 * Created by stefan on 16.01.17.
 */
@SuppressWarnings("ALL")
public class CSVParserTest {

    @Test
    public void parseStringAndAddToProvider() throws Exception {
        NumberplateProvider provider = NumberplateProvider.createNumberplateProvider();
        String testCSVFile = "BB,Böblingen,Baden-Württemberg\n"
                + "BC,Biberach/Riß,Baden-Württemberg\n"
                + "BD,\"Bundestag, Bundesrat, Bundesregierung\",\n"
                + "BGL,Berchtesgadener Land,Bayern";
        CSVParser parser = new CSVParser(testCSVFile);
        parser.parseStringAndAddToProvider(); // parser is singleton and final member of CSVParser

        int sizeOfNumberplates = provider.provide().size();

        boolean hasContent = false;
        if (sizeOfNumberplates > 0)
            hasContent = true;
        assertEquals(hasContent, true);

        for (Numberplate n : provider.provide()) {
            assertEquals(false, n.toDebugString().contains("\""));
        }

    }

}