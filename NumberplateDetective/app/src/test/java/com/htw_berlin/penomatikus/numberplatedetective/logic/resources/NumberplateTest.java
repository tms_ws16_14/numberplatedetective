package com.htw_berlin.penomatikus.numberplatedetective.logic.resources;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Test for the Numberplate class
 * All warnings are suppressed because this is a test
 * Created by stefan on 16.01.17.
 */
@SuppressWarnings("ALL")
public class NumberplateTest {

    @Test
    public void getCounty() throws Exception {
        Numberplate testObject = new Numberplate("A", "Augsburg", "Bayern");
        String result = testObject.getCounty();
        assertEquals("Augsburg", result);
    }

    @Test
    public void getLetters() throws Exception {
        Numberplate testObject = new Numberplate("A", "Augsburg", "Bayern");
        String result = testObject.getLetters();
        assertEquals("A", result);
    }

    @Test
    public void getFederalState() throws Exception {
        Numberplate testObject = new Numberplate("A", "Augsburg", "Bayern");
        String result = testObject.getFederalState();
        assertEquals("Bayern", result);
    }

    @Test
    public void isDiplomatic() throws Exception {
        Numberplate testObject = new Numberplate("A", "Augsburg", "Bayern");
        testObject.setDiplomatic();
        boolean result = testObject.isDiplomatic();
        assertEquals(true, result);
    }

    @Test
    public void isDiplomaticByConstructor() throws Exception {
        Numberplate testObject = new Numberplate("160", "Usbekistan", "");
        boolean result = testObject.isDiplomatic();
        assertEquals(true, result);
    }

    @Test
    public void isNotDiplomaticByConstructor() throws Exception {
        Numberplate testObject = new Numberplate("A", "Augsburg", "Bayern");
        boolean result = testObject.isDiplomatic();
        assertEquals(false, result);
    }

    @Test
    public void setDiplomatic() throws Exception {
        isDiplomatic();
    }

    @Test
    public void toDebugString() throws Exception {
        Numberplate testObject = new Numberplate("A", "Augsburg", "Bayern");
        testObject.setDiplomatic();
        String result = testObject.toDebugString();
        assertEquals(":: Numberplate: A : Augsburg : Bayern : true", result);
    }

}